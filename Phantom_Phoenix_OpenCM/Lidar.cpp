#if ARDUINO > 99
#include <Arduino.h> // Arduino 1.0
#else
#include <Wprogram.h> // Arduino 0022
#endif

#include "_Lidar.h"

// Constructor
Lidar::Lidar()
{
    m_pulseWidth = 0;
}

unsigned long Lidar::GetReading()
{
    unsigned long reading = pulseIn(8, HIGH);
    m_pulseWidth = reading / 10; // 10usec = 1 cm of distance
    return m_pulseWidth;
}

void Lidar::Begin()
{
    Serial.begin(115200);
    pinMode(LIDAR_OUTPUT_PIN, OUTPUT);
    digitalWrite(9, LOW);
    pinMode(LIDAR_INPUT_PIN, INPUT);
}