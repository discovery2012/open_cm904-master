#ifndef LIDAR_H
#define LIDAR_H

#define LIDAR_OUTPUT_PIN 9
#define LIDAR_INPUT_PIN 8

class Lidar
{
  public:
    Lidar();
    void Begin();
    unsigned long GetReading();

  private:
    unsigned long m_pulseWidth;
};

#endif